<?php

if ( ! function_exists('multi_captcha')) {

    /**
     * @param string $config
     * @param null $name
     * @return mixed
     */
    function multi_captcha($config = 'default', $name = 'main')
    {
        return app('captcha')->create($config, $name);
    }
}

if ( ! function_exists('multi_captcha_src')) {
    /**
     * @param string $config
     * @param null $name
     * @return string
     */
    function multi_captcha_src($config = 'default', $name = 'main')
    {
        return app('captcha')->src($config, $name);
    }
}

if ( ! function_exists('multi_captcha_img')) {

    /**
     * @param string $config
     * @param null $name
     * @return mixed
     */
    function multi_captcha_img($config = 'default', $name = 'main')
    {
        return app('captcha')->img($config, $name);
    }
}

if ( ! function_exists('multi_captcha_check')) {
    /**
     * @param $value
     * @param null $name
     * @return bool
     */
	function multi_captcha_check($value, $name = 'main')
	{
		return app('captcha')->check($value, $name);
	}
}

if ( ! function_exists('multi_captcha_api_check')) {
	/**
	 * @param $value
	 * @return bool
	 */
	function multi_captcha_api_check($value, $key)
	{
		return app('captcha')->check_api($value, $key);
	}
}
