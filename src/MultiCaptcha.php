<?php
/**
 * Created by PhpStorm.
 * User: Alireza
 * Date: 3/6/2019
 * Time: 11:01 AM
 */

namespace Arasharg\Captcha;


use Mews\Captcha\Captcha;

class MultiCaptcha extends Captcha
{

    protected $name;

    /**
     * Create captcha image
     *
     * @param string $config
     * @param bool $api
     * @return array|mixed
     * @throws \Exception
     */
    public function create(string $config = 'default', $name = null, $api = false)
    {
        $this->name = $name;

        return parent::create($config, $api);
    }

    /**
     * Generate captcha text
     *
     * @return array
     * @throws \Exception
     */
    protected function generate(): array
    {
        $bag = parent::generate();

        $this->session->put('captcha-' . $this->name, [
            'sensitive' => $bag['sensitive'],
            'key'       => $bag['key']
        ]);

        return $bag;
    }

    /**
     * Captcha check
     *
     * @param string $value
     * @param null $name
     * @return bool
     */
    public function check($value, $name = null): bool
    {
        if(! $name) return false;

        $str = "captcha-{$name}";

        if ( ! $this->session->has($str))
        {
            return false;
        }

        $key = $this->session->get("{$str}.key");
        $sensitive = $this->session->get("{$str}.sensitive");

        if ( ! $sensitive)
        {
            $value = $this->str->lower($value);
        }


        $res = $this->hasher->check($value, $key);
        //  if verify pass,remove session
        if ($res) {
            $this->session->remove($str);
        }

        return $res;
    }

    /**
     * Generate captcha image source
     *
     * @param string $config
     * @param null $name
     * @return string
     */
    public function src($config = null, $name = null): string
    {
        list($link, $random) = explode('?',parent::src($config));

        return $link . '?' . config('captcha.token_name','_ctk') . '=' . $this->makeToken($name) . '&' . $random;
    }

    /**
     * Generate captcha image html tag
     *
     * @param string $config
     * @param null $name
     * @param array $attrs
     * $attrs -> HTML attributes supplied to the image tag where key is the attribute and the value is the attribute value
     * @return string
     */
    public function img($config = null, $name = null, $attrs = []): string
    {
        $attrs_str = '';
        foreach($attrs as $attr => $value){
            if ($attr == 'src'){
                //Neglect src attribute
                continue;
            }
            $attrs_str .= $attr.'="'.$value.'" ';
        }
        return '<img src="' . $this->src($config, $name) . '" '. trim($attrs_str).'>';
    }


    /**
     *
     *
     * @param string $name
     * @return string
     */
    public function makeToken($name)
    {
        do
        {
            $token = md5(request()->fullUrl().$this->str->random(8).($name ?: 'main'));

        } while($this->session->has($token));

        $this->session->put('captcha-'.$token);

        return $token;
    }

}