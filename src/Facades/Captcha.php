<?php

namespace Arasharg\Captcha\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Arasharg\Captcha
 */
class Captcha extends Facade {

    /**
     * @return string
     */
    protected static function getFacadeAccessor() { return 'captcha'; }

}
